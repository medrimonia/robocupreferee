/*****************************************************************************/
/*! \file    Team.cpp
 *  \author  LH
 *  \date    2011-05-26
 *
 *  \brief   Team informations sent by the referee box
 *****************************************************************************/
#include "Team.h"

using namespace std;

namespace RhobanReferee{

    Team::Team(){
        m_team_number = 0;
        m_team_color = 0;
        m_goal_color = 0;
        m_score = 0;
    }

    Team::~Team(){
    }

    int Team::getTeamNumber() const{
        return m_team_number;
    }

    int Team::getTeamColor() const{
        return m_team_color;
    }

    int Team::getGoalColor() const{
        return m_goal_color;
    }

    int Team::getScore() const{
        return m_score;
    }

    int Team::getNbRobots() const{
        return NB_ROBOTS;
    }

    const Robot & Team::getRobot(int robot) const{
        return m_robots[robot];
    }

    /* Use a broadcasted message to update the Robot */
    void Team::update_from_message(char const* message,int numTeam){
        int d = Constants::nb_chars_by_team * numTeam;//decalage
        m_team_number = chars_to_int(message, 20 + d, 21 + d);
        m_team_color = chars_to_int(message, 21 + d, 22 + d);
        m_goal_color = chars_to_int(message, 22 + d, 23 + d);
        m_score = chars_to_int(message, 23 + d, 24 + d);
        for (int robot = 0; robot < NB_ROBOTS; robot++){
            m_robots[robot].update_from_message(message, numTeam, robot);
        }
    }

    ostream& operator<<(ostream& flux, Team const& t){
        flux << '\t' << "team_number : " << t.getTeamNumber() << endl;
        flux << '\t' << "team_color : " << t.getTeamColor() << endl;
        flux << '\t' << "goal_color : " << t.getGoalColor() << endl;
        flux << '\t' << "score : " << t.getScore() << endl;
        for (int robot = 0; robot < NB_ROBOTS; robot++){
            flux << '\t' << "robot " << robot << std::endl;
            flux << t.getRobot(robot);
        }
        return flux;
    }
}

/*****************************************************************************/
/*****************************************************************************/
